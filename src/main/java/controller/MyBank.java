package controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import model.*;

import view.Cli;
import view.Gui;
import view.SplashScreen;

public class MyBank {

    private Lang lang;
    private DBase dBase;
    private Kunde kunde;
    private Konto konto;
    private BankLogic bankLogic;
    private BankSettings bankSettings;

    public MyBank(String variante) {
        // TODO: for release!
        if (!MyBankEngine.checkVersion())
            return;

        // Everything for configuration
        bankSettings = BankSettings.getInstance();

        //Business logic
        bankLogic = BankLogic.getInstance();
        lang = new Lang(bankSettings.getSlanguage());
        System.out.println(lang.size() + " " + lang.getLangStr());

        if (variante.equals("console")) {
            new Cli(this);
        }

        if (variante.equals("desktop")) {
            MyBankEngine.oneInstance();

            Thread t1 = new Thread(new SplashScreen(8));
            t1.setPriority(Thread.MIN_PRIORITY);
//            t1.start();
//            new Gui(this);
//            new MyLocales();
            Thread t2 = new Thread(new Gui(this));
            t2.setPriority(Thread.MAX_PRIORITY);

            t1.start();
            t2.start();
            /*
            try {
                t1.join();
                t2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

             */
        }
    }

    /* @TODO: main bei release entfernen */
    public static void main(String[] args) {
        // Bei Start ohne Main-class
        // new MyBank("console");
        new MyBank("desktop");
    }

    public String trans(String key) {
        return lang.getValue(key);
    }

    public boolean connected() {
        return dBase != null && dBase.isOnline();
    }

    public String connectedStr() {
        return (connected()) ? trans("connted") : trans("unconnted");
    }

    public boolean isLoggedIn() {
        return kunde != null;
    }

    public int dbLogin(String user, String pass) {
        // Initialisiere dBase, Objekt wird erzeugt
        dBase = new DBase(bankSettings.getDbSettings());
        /*
         * Führt den Verbindungstest durch, falls noch nicht geschen Diese
         * Methode ändert eine Variable, die besagt, ob Zugang möglich ist
         *
         * Wenn vorher der Verbindungstest manuell durchgeführt wurde, wird er
         * an dieser Stelle übersprungen
         */
        if (!dBase.isOnline()) {
            dBase.connectTest();
        }

        // Verbindung zu DB gestört
        if (!connected()) {
            return 1;
        }

        // Ab hier ist die Verbindung i.O.
        // Prüfe Logindaten
        if (dBase.userConn(user, pass)) {
            kunde = new Kunde(user, dBase, this);
            return 0;
        }

        // User / Pw Kombi nicht ok
        return 2;
    }

    public void dbLogoff() {
        dBase.close();
        dBase = null;
        // Kundendaten freisetzen
        kunde = null;
    }

    public void quit() {
        if (dBase != null) {
            dbLogoff();
        }
        System.exit(0);
    }

    // TODO: Logik einbauen
    public String barZahlung(int richtung, String sbetrag) {
        double dbetrag;
        if (sbetrag.contains(","))
            sbetrag = sbetrag.replace(',', '.');
        try {
            dbetrag = Double.parseDouble(sbetrag);
        } catch (Exception e) {
            return trans("wrongtrans");
        }

        StringBuilder result = new StringBuilder();
        double max = Double.valueOf(bankLogic.get("zahlung"));
        if (dbetrag >= max) {
            result.append("\n" + lang.getValue("toohigh") + " " + dbetrag);
            return result.toString();
        }
        if (dbetrag >= 30000 && dbetrag < max)
            result.append("\n" + lang.getValue("tax30"));

        if (richtung == 1) {
            if (konto.einzahlen(dbetrag))
                result.append("\n").append(lang.getValue("income"));
        }

        if (richtung == 0) {
            if (konto.auszahlen(dbetrag))
                result.append("\n").append(lang.getValue("outcome"));
        }

        /*
         * if(rich==1) { if(konto.konto_typ().equals("FEST")) {
         * mtextarea.append(
         * "\nKeine Einzahlung auf laufende Festgeldkonten m�glich"); }
         * if(konto.einzahlen(betrag))mtextarea.append(
         * "\nEinzahlvorgang abgeschlossen!"); }
         *
         * if(richtung==0&&help!=9999) {
         * if(konto.auszahlen(betrag))mtextarea.append(
         * "\nAuszahlvorgang abgeschlossen!"); }
         */
        result.append("\n").append(lang.getValue("transfinish"));
        return result.toString();
    }

    public String k_getFullname() {
        return kunde.getFullname();
    }

    public String k_getArbeit() {
        String datum = kunde.getArbeit();
        String date = trans("dateformat");
        String arbeitfor = "nodate";
        try {
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(datum);
            arbeitfor = new SimpleDateFormat(date).format(d);
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        return arbeitfor;
    }

    public boolean k_setArbeit(String datum) {
        if (k_getArbeit().equals(datum))
            return true;

        String formArbeit;
        try {
            String date = trans("dateformat");
            // String date="dd.MM.yyyy";
            // if(progLang.equals("en"))date="MM/dd/yyyy";
            Date d = new SimpleDateFormat(date).parse(datum);
            formArbeit = new SimpleDateFormat("yyyy-MM-dd").format(d);
        } catch (ParseException e1) {
            return false;
        }
        try {
            if (kunde.setArbeit(formArbeit))
                return true;
        } catch (Exception e) {
            return false;
        }
        // Default: false
        return false;
    }

    public int k_anzahlKonten() {
        return kunde.anzahlKonten();
    }

    public double k_gesamtSaldo() {
        return kunde.getGesamtsaldo();
    }

    public long[] k_getNummern() {
        return kunde.getNummern();
    }

    public String[] k_listeKontos() {
        return kunde.listeKontos();
    }

    // @TODO
    public void ko_createKonto(int typ, String a, String b) {
        Double da = 0.0, db = 0.0;
        try {
            da = Double.valueOf(a);
            db = Double.valueOf(b);
        } catch (Exception e) {
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            } catch (Exception ef) {
                ef.printStackTrace();
            }
            JOptionPane.showMessageDialog(null, "Probleme bei Eingabe der Werte!", "Fehler 199",
                    JOptionPane.INFORMATION_MESSAGE);
        }

        // Bedingungen prüfen
        if (kunde.anzahlKonten() == 5 || kunde.countKonto("SPAR") == 1 && typ == 2
                || kunde.countKonto("FEST") == 1 && typ == 3) {
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            } catch (Exception ef) {
                ef.printStackTrace();
            }
            JOptionPane.showMessageDialog(null, "Maximale Anzahl Konten dieser Art erreicht!", "Fehler 555",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            konto = new Konto(dBase);
            String bub;
            switch (typ) {
                case 1:
                    bub = "GIRO";
                    break;
                case 2:
                    bub = "SPAR";
                    break;
                case 3:
                    bub = "FEST";
                    break;
                default:
                    bub = "unbekannt";
            }
            String bla = bub;
            Double luba = db;
            luba = db;
            /* @TODO */
            /*
            if (konto.kontoCreate(bub, da, db, kunde.getID())) {
                // mtextarea.append("\nKonto mit Nr. "+konto.getNr()+" wurde
                // erstellt!");
                // temp.dispose();
            }
            */
        }
    }

    public long ko_getNr() {
        return konto.getNr();
    }

    public void ko_choose(long nr) {
        konto = new Konto(dBase);
        konto.getData(nr);
    }

    public String ko_kontoinfo() {
        return konto.kontoinfo();
    }

    public String ko_umsatz() {
        return konto.umsatz();
    }

    public String connTest() {
        if (dBase == null)
            dBase = new DBase(bankSettings.getDbSettings());
        return dBase.connectTest();
    }

    public Lang getLang() {
        return lang;
    }

    public void setLang(String language) {
        lang = new Lang(language);
    }

    public BankSettings getBankSettings() {
        return bankSettings;
    }

    public void saveSettings(String[] data) {
        bankSettings.setSettings(data);
        bankSettings.saveSettings();
    }
}