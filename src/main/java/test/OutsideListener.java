package test;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import model.Lang;
import view.LoginDialog;

public class OutsideListener extends JFrame  {
	private static final long serialVersionUID = 5114606205798857643L;
	private JTextField textField = new JTextField(10);
	private JButton showDialogBtn = new JButton("Show Dialog");
	
	private LoginDialog myDialog;
	private LoginDialog tempi;
	private static JFrame frame;

	public void buildScreen() {

	}

	public OutsideListener(String title) {
		super(title);
		textField.setEditable(false);
		
		Lang la = new Lang("de");
		myDialog = new LoginDialog(this, la);
		myDialog.setVisible(false);
		
		showDialogBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// !! add a listener to the dialog's button

				if (!myDialog.isVisible()) {
					myDialog.setVisible(true);
				}
			}
		});

		myDialog.addConfirmListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("LEBT");
				String text = myDialog.getUsername();
				textField.setText(text);
				myDialog.dispose();
			}
		});

		

		JPanel panel = new JPanel();
		panel.add(textField);
		panel.add(showDialogBtn);

		add(panel);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(400, 300);
	}

	private static void createAndShowGui() {
		frame = new OutsideListener("Nur ein Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
			}
		});
	}

}