package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class First {

	private JFrame frame;
	private JTextField field;
	private JButton showDialogBtn;
	Second myDialog ;

	public First() {
		frame = new JFrame("Laber");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setBounds(70, 70, 700, 380);

		showDialogBtn = new JButton("Show Dialog");
		field = new JTextField(10);

		JPanel panel = new JPanel();
		panel.add(field);
		panel.add(showDialogBtn);

		frame.add(panel);
		frame.pack();

		showDialogBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logonScreen();
			}
		});
	}

	public void logonScreen() {
		myDialog = new Second(frame, "ZWEITER");
		myDialog.setAlwaysOnTop(true);
		myDialog.setVisible(true);
		
		myDialog.addConfirmListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("LEBT");
				String text = myDialog.getTextFieldText();
				field.setText(text);
				myDialog.dispose();
			}
		});

	}

	public static void main(String[] args) {
		First a = new First();		
	}

}
