package test;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

class MyDialog extends JDialog {
	   private static final long serialVersionUID = 5601630448883457841L;
	private JTextField textfield = new JTextField(10);
	   private JButton confirmBtn = new JButton("Confirm");

	   public MyDialog(JFrame frame, String title) {
	      super(frame, title, false);
	      JPanel panel = new JPanel();
	      panel.add(textfield);
	      panel.add(confirmBtn);

	      add(panel);
	      pack();
	      setLocationRelativeTo(frame);
	   }

	   public String getTextFieldText() {
	      return textfield.getText();
	   }

	   public void addConfirmListener(ActionListener listener) {
	      confirmBtn.addActionListener(listener);
	   }
	}