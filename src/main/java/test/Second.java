package test;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Second extends JFrame {

	private static final long serialVersionUID = -8234014626698439047L;
	private JTextField textfield = new JTextField(10);
	private JButton confirmBtn = new JButton("Confirm");

	public Second(JFrame frame, String title) {
		//super(frame, title, false);
		JPanel panel = new JPanel();
		panel.add(textfield);
		panel.add(confirmBtn);

		add(panel);
		pack();
		setLocationRelativeTo(frame);
	}

	public String getTextFieldText() {
		return textfield.getText();
	}

	public void addConfirmListener(ActionListener listener) {
		confirmBtn.addActionListener(listener);
	}
}
