package au_opencsv;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import model.Lang;

public class Zweilesen {

    private Lang la;

    public Zweilesen() {
        la = new Lang("de");
        getFrom("xa", 1);
        la.save("de");
        System.out.println(la.size() + " de");

        la = new Lang("en");
        getFrom("xa", 2);
        la.save("en");
        System.out.println(la.size() + " en");
    }

    public static void main(String[] args) {
        new Zweilesen();
    }

    private void getFrom(String language, int spalte) {
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader("liste_" + language + ".csv"), ';');
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        String[] nextLine;

        try {
            while ((nextLine = reader.readNext()) != null) {
                // nextLine[] is an array of values from the line
                la.add(nextLine[0], nextLine[spalte]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
