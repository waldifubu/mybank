package bastel;

final public class Node<T>
{
	
	private Node<T> left, right;
	
	@SuppressWarnings("unused")
	/**
	 * Element im Knoten, praktisch kann das alles sein
	 */
	private T element;
	
	public Node(T element)
	{
		this.element = element;
	}
	
	public void setLeft(final Node<T> left)
	{
		this.left = left;
	}
	
	public void setRight(final Node<T> right)
	{
		this.right = right;
	}
	
	public Node<T> getLeft()
	{
		return left;
	}
	
	public Node<T> getRight()
	{
		return right;
	}
	
}

