package bastel;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLWrite {
    public static void main(String[] args)  throws Exception {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();
       
        Element elem = doc.createElement("mybank");
        //el.setTextContent("spar");
        
        doc.appendChild(elem);
        
        Element e1 = doc.createElement("spar");
        e1.setAttribute("para1", "10");
        e1.setAttribute("para2", "20");
        
        elem.appendChild(e1);

     
        DOMSource source = new DOMSource(doc);     
        PrintStream ps = new PrintStream("test2.xml");
        
        StreamResult result = new StreamResult(ps);
     
                      
        Transformer tr = TransformerFactory.newInstance().newTransformer();
        
        tr.setOutputProperty(OutputKeys.INDENT, "yes");        
        tr.setOutputProperty(OutputKeys.METHOD, "xml");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        //tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
              
        tr.transform(source, result);
        
        File file = new File("test2.xml");
                
		FileInputStream fis = new FileInputStream(file);

		int oneByte;
		while ((oneByte = fis.read()) != -1) {
			System.out.write(oneByte);
			// System.out.print((char)oneByte); // could also do this
		}
		fis.close();
		System.out.flush();
    }
}