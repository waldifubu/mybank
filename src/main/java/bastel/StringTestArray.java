package bastel;

import java.util.Arrays;
import java.util.List;

public class StringTestArray  
{  
   public static void main(String[] args)  
   {  
      String[] word = {"ace", "boom", "crew", "dog", "eon"};  

      List<String> wordListe = Arrays.asList(word);  

      for (String e : wordListe)  
      {  
         System.out.println(e);  
      }  
      
   }
 
}