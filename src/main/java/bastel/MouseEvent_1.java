package bastel;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;

public class MouseEvent_1 extends JFrame {

	private static final long serialVersionUID = 1L;
	/* Labels stehen nachher links */
	JLabel klickL = new JLabel("Klick:");
	JLabel linksklickL = new JLabel("Linksklick:");
	JLabel rechtsklickL = new JLabel("Rechtsklick:");
	JLabel loslassenL = new JLabel("Loslassen:");
	JLabel eintrittL = new JLabel("Eintritt:");
	JLabel austrittL = new JLabel("Austritt:");
	JLabel ziehenL = new JLabel("Ziehen:");
	JLabel bewegenL = new JLabel("Bewegen:");
	JLabel mausradL = new JLabel("Mausrad:");

	/* TextFelder steher nachher rechts */
	JTextField klickT = new JTextField();
	JTextField linksklickT = new JTextField();
	JTextField rechtsklickT = new JTextField();
	JTextField loslassenT = new JTextField();
	JTextField eintrittT = new JTextField();
	JTextField austrittT = new JTextField();
	JTextField ziehenT = new JTextField();
	JTextField bewegenT = new JTextField();
	JSlider mausradS = new JSlider();

	int mc = 0;
	int rc = 0;
	int mrc = 0;
	int lc = 0;
	int ll = 0;
	int et = 0;
	int at = 0;

	// bewegung
	int zi = 0;
	int be = 0;

	/* JPANEL */
	JPanel tab = new JPanel();

	public MouseEvent_1(String titel) {
		super(titel);

		tab.setLayout(new GridLayout(11, 2, 0, 0));
		tab.add(klickL);
		tab.add(klickT);
		tab.add(linksklickL);
		tab.add(linksklickT);
		tab.add(rechtsklickL);
		tab.add(rechtsklickT);
		tab.add(loslassenL);
		tab.add(loslassenT);
		tab.add(eintrittL);
		tab.add(eintrittT);
		tab.add(austrittL);
		tab.add(austrittT);
		tab.add(ziehenL);
		tab.add(ziehenT);
		tab.add(bewegenL);
		tab.add(bewegenT);
		tab.add(mausradL);
		tab.add(mausradS);
		this.add(tab);

		this.getGlassPane().setVisible(true);
		this.getGlassPane().addMouseListener(new MausEreignisse());
		this.getGlassPane().addMouseMotionListener(new Mausbewegen());
		this.getGlassPane().addMouseWheelListener(new Mausrad());

	} // end MouseEvent

	public class MausEreignisse implements MouseListener {
		public void mouseClicked(MouseEvent e) {

			mc++;
			klickT.setText("" + mc);
			// klickT.setText(klickT.getText()+1);

		} // end mouseClick

		public void mousePressed(MouseEvent e) {
			int pressed = e.getButton();

			if (pressed == 1) {
				lc++;
				linksklickT.setText("" + lc);
			} else if (pressed == 2) {
				mrc++;
			} else {
				rc++;
				rechtsklickT.setText("" + rc);
			} // end else

		} // end mousePressed

		public void mouseExited(MouseEvent e) {
			at++;
			austrittT.setText("" + at);
		} // end mousePressed

		public void mouseEntered(MouseEvent e) {
			et++;
			eintrittT.setText("" + at);
		} // end mousePressed

		public void mouseReleased(MouseEvent e) {
			ll++;
			loslassenT.setText("" + ll);
		} // end mousePressed

	} // end class MouseListener

	public class Mausbewegen implements MouseMotionListener {
		public void mouseMoved(MouseEvent e) {
			be++;
			bewegenT.setText("" + be);

		} // end mouseMoves

		public void mouseDragged(MouseEvent e) {
			zi++;
			ziehenT.setText("" + zi);
		} // end mouseDragged

	} // end MouseMotionListener

	public class Mausrad implements MouseWheelListener {
		public void mouseWheelMoved(MouseWheelEvent e) {
			mausradS.setValue(mausradS.getValue() + e.getWheelRotation());
		} // end mouseWheelMoved
	} // end Mausrad

	public static void main(String args[]) {

		MouseEvent_1 fenster = new MouseEvent_1("Maus-Ereignisse");

		fenster.setVisible(true);
		fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenster.setResizable(false);
		fenster.setSize(480, 480);

	} // end main
} // end class
