package bastel;

import java.io.InputStream;

public class ResourceBeispiel {

    public InputStream is;

    public ResourceBeispiel() {
        is = getClass().getClassLoader().getResourceAsStream("lang/liste_xa.csv");
    }

    public static void main(String[] args) {
        InputStream stream = ResourceBeispiel.class.getResourceAsStream("/lang/liste_xa.csv");
        System.out.println(stream != null);

        ResourceBeispiel resourceBeispiel = new ResourceBeispiel();
        stream = resourceBeispiel.is;
        //((FileInputStream) ((BufferedInputStream) stream).in).path

        System.out.println(stream != null);
    }

    //Lang.class.getClassLoader().getResourceAsStream("lang/liste_xa.csv")
}
