package bastel;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class AmpelMain extends JFrame {
	private static final long serialVersionUID = 1L;


	int akt = 0;
	 
	
	Container cont; 
	ZeichenPanel z;
	
	JButton schritteB = new JButton("Einzelschritte");
	JButton endeB = new JButton("Ende");
	JPanel buttonsP = new JPanel();
	
	/*
	public static  int getAkt(){
		return this.akt;
	} // end getAkt
	*/
	public void setAkt(int akt){
		this.akt = akt;
	} // end setAkt
	public int getAkt(){
		return akt;
	} // end getAkt
	
	public AmpelMain(){
		cont = getContentPane();
		z = new ZeichenPanel(1);
		cont.add(z);
		
		this.setLayout(new BorderLayout());
		this.add(z, BorderLayout.CENTER);
		buttonsP.add(schritteB, BorderLayout.WEST);
		buttonsP.add(endeB, BorderLayout.EAST);
		this.add(buttonsP, BorderLayout.SOUTH);
		
		schritteB.addActionListener(new Button_fkt());
		endeB.addActionListener(new Button_fkt());
			
	} // end AmpelMain
	class Button_fkt implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getActionCommand().equals("Einzelschritte")){
				setAkt(getAkt()+ 1);
				System.out.println(getAkt());
			} // end if
			else{
				System.exit(0);
			} // end else
		} // end actionPerformed
	} // end class
	
	public static void main(String[] args) {
		AmpelMain fenster = new AmpelMain();
		fenster.setSize(600,450);
		fenster.setLocation(200,200);
		fenster.setVisible(true);
		fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		

	}

}
