package view;

import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

import model.MyConstants;

public class AboutDialog extends JDialog {
	
	private static final long serialVersionUID = 4324291834102356825L;

	public AboutDialog(JFrame mainWindow) {
		super(mainWindow, "About", true);		
		Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource(MyConstants.getImagePath() + "info2.png"));
		setIconImage(img);
		ImageIcon icon = new ImageIcon(getClass().getResource(MyConstants.getImagePath() + "mybank.jpg"), "Zur Information");
		JLabel textforinfo = new JLabel("<html><b>Verwendung auf eigene Gefahr!</b><br>"
				+ "Dieses Projekt ist durch Langeweile entstanden.<br>" + "Eigentümer der Software ist:<br>"
				+ "<big>Waldemar Dell</big><br/>&copy; 2012 Version 1.0</html>", icon, JLabel.CENTER);
		add(textforinfo);
		setResizable(false);
		setBounds(mainWindow.getX(), mainWindow.getY(), 400, 200);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}
}
