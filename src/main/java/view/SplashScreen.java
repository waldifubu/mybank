package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;

import model.MyConstants;

public class SplashScreen extends JWindow implements Runnable {

    private static final long serialVersionUID = 1L;
    private JProgressBar bar;

    public SplashScreen(int duration) {
        setAlwaysOnTop(true);
        bar = new JProgressBar(0, duration * 1000);
        bar.setStringPainted(true);
    }

    @Override
    public void run() {
        JPanel content = (JPanel) getContentPane();
        int width = 500;
        int height = 340;
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screen.width - width) / 2;
        int y = (screen.height - height) / 2;
        setBounds(x, y, width, height);
        JLabel image = new JLabel(new ImageIcon(getClass().getResource(MyConstants.getImagePath() + "splash.jpg")));
        //Color farbe = new Color(122, 220, 220, 255);

        content.add(image, BorderLayout.CENTER);
        content.add(bar, BorderLayout.SOUTH);

        content.setBorder(BorderFactory.createLineBorder(Color.black, 2, true));
//	    content.setBorder(BorderFactory.createEtchedBorder(1,Color.black,Color.white));
        setVisible(true);
        pack();
        try {
            //Thread.sleep(duration);
            for (int i = 1; i <= bar.getMaximum(); ++i) {
                bar.setValue(i);
                Thread.yield();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        setAlwaysOnTop(false);
        dispose();
    }

}
