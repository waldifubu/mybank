package view;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import model.Lang;
import model.MyConstants;

public class LoginDialog extends ScreenDialog {

    private static final long serialVersionUID = -6485860171742856288L;

    // Logindata
    private JTextField felduser;
    private JPasswordField feldpass;
    private JButton confirmBtn;

    public LoginDialog(JFrame mainWindow, Lang my) {
        Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource(MyConstants.getImagePath() + "euro.png"));
        setIconImage(img);
        setTitle(my.getValue("login"));
        setAlwaysOnTop(true);
        setVisible(true);

        getContentPane();
        setLayout(new GridLayout(3, 2));
        add(new JLabel("  " + my.getValue("user"), SwingConstants.LEFT));
        felduser = new JTextField(20);
        felduser.requestFocus();

        felduser.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ESCAPE) {
                    dispose();
                }
            }
        });

        add(felduser);
        add(new JLabel("  " + my.getValue("passwort"), SwingConstants.LEFT));
        feldpass = new JPasswordField(20);

        add(feldpass);
        add(new JLabel(" "));
        confirmBtn = new JButton(my.getValue("connect"));
        add(confirmBtn);

        feldpass.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ESCAPE) {
                    dispose();
                }
                if (key == KeyEvent.VK_ENTER) {
                    if (!felduser.getText().isEmpty() && !new String(feldpass.getPassword()).isEmpty()) {
                        confirmBtn.doClick();
                    }
                    // Fürs Archiv, gleiches Resultat (Wir emulieren den Klick)
                    // wie:
                    // actionPerformed(new
                    // ActionEvent(this,ActionEvent.ACTION_PERFORMED,
                    // "logon_connect"));
                }
            }
        });
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(mainWindow.getX(), mainWindow.getY(), 220, 100);
        setVisible(true);
    }

    public void addConfirmListener(ActionListener listener) {
        confirmBtn.addActionListener(listener);
    }

    public String getUsername() {
        return felduser.getText();
    }

    public String getUserPassword() {
        return new String(feldpass.getPassword());
    }
}
