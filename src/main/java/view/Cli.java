package view;

import java.util.Scanner;

import controller.MyBank;

public class Cli {
    //  Kontrollklasse
    private MyBank my;
    String a;
    String b;

    private Scanner in = new Scanner(System.in);

    public Cli(MyBank a) {
        my = a;
        menue();
        eingabe();
    }

    public void menue() {
        System.out.println(my.trans("welcomever"));
        System.out.println("=================================================");
        System.out.println("Status: " + my.connectedStr());
        System.out.println("menu - blendet dieses Menü ein");
        System.out.println("on   - Anmelden");
        System.out.println("off  - Abmelden");

        if (my.isLoggedIn()) {
            System.out.println("1    - Kontowahl");
            System.out.println("2    - Konto anlegen");
            System.out.println("3    - Konto auflösen");
            System.out.println("4    - Einzahlen");
            System.out.println("5    - Auszahlen");
            System.out.println("6    - Überweisung");
            System.out.println("7    - Umsätze");
            System.out.println("8    - Kontoinfo");
            System.out.println("9    - Festgeldkonto");
            System.out.println("10   - Sparbuch");
            System.out.println("11   - Arbeitsdatum");
        }
        System.out.println("x    - Beenden");
    }

    private void eingabe() {
        String a;
        do {
            System.out.print("\nmyBank> ");
            a = in.nextLine();
            exek(a);
        }
        while (a != "x");
    }

    private void exek(String ein) {
        if (ein.equals("")) eingabe();
        if (ein.equals("menu")) menue();
        if (ein.equals("x")) {
            System.out.println("Bye");
            my.quit();
        }
        if (ein.equals("conntest")) {
            System.out.println("\n" + my.trans("connmess") + "\n" + my.trans("connmess2") + "\n");
            System.out.println(my.connTest());
        }
        if (ein.equals("on")) login();
        if (ein.equals("off")) logout();
        if (ein.equals("saldo")) System.out.println(my.k_gesamtSaldo() + " Euro");
    }

    private void logout() {
        System.out.println(my.trans("user") + " " + my.k_getFullname() + " " + my.trans("logofft") + "\n" + my.trans("welcomever"));
        my.dbLogoff();
    }

    private void login() {
        System.out.print(my.trans("user") + ": ");
        a = in.next();
        System.out.print(my.trans("passwort") + ": ");
        b = in.next();
        int lev = my.dbLogin(a, b);
        if (lev == 0) {
            System.out.print(my.trans("user") + " " + my.k_getFullname() + " " + my.trans("userconn"));
        }
    }

}
