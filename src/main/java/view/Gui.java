package view;

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import controller.EingabeCheck;
import controller.MyBank;
import model.MyConstants;
import model.MyLocales;

/**
 * Should be Gui, but acts like a controller for Dialogs, so it's the glue
 */
public class Gui implements ActionListener, Runnable {
    // Für settings
    public JComboBox<String> dropDown;
    public JTextField dbdriver;
    public JTextField dbhost;
    public JTextField a;
    public JTextField b;
    public JTextField dbport;
    public JTextField dbuser;
    public JPasswordField dbpass;
    public JTextField dbdatabase;
    // Kontrollklasse
    private MyBank my;
    // Hauptfenster und HilfstempFrame
    private JFrame mainWindow;
    private JDialog temp;
    private JPanel pane;
    private JTextField console;
    private JToolBar toolbar;
    private JLabel status;
    // Textfeld
    private JTextArea mtextarea;
    // Wurde Konto gewählt?
    private boolean kontoChosen;


    private int help;

    public Gui(MyBank mybank) {
        my = mybank;
    }

    private void createAndShowGUI() {
        // Primärframe wird geladen
        mainWindow = new JFrame("myBank v1.0");
        Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource(MyConstants.getImagePath() + "euro.png"));

        // Windows-Optik
        mainWindow.setIconImage(img);
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        // Blendet das Menu ein. Umleitung ist lediglich, um den ganzen Code
        // auszulagern
        JMenuBar menu = menue();

        // Linke Toolbar, für Bankfunktionen
        toolbar = new JToolBar(my.trans("func"), JToolBar.VERTICAL);
        toolbar.setVisible(false);

        // Grosses Textfeld erzeugen
        mtextarea = new JTextArea();
        mtextarea.setFont(new Font("Calibri", Font.PLAIN, 16));
        mtextarea.setText(my.trans("welcomever"));
        mtextarea.setEditable(false);
        mtextarea.setLineWrap(true);
        mtextarea.setBorder(BorderFactory.createLoweredBevelBorder());
        mtextarea.setCaretPosition(mtextarea.getDocument().getLength() );

        // Border nur für Optik
        Border compound = BorderFactory.createCompoundBorder(BorderFactory.createLoweredBevelBorder(),
                BorderFactory.createLineBorder(Color.GRAY));

        // Eingabetextfeld "console" mit split Komponente
        console = new JTextField();
        console.setVisible(false);
        console.setBorder(compound);
        console.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ENTER) {
                    exek(console.getText().toString());
                }
            }
        });

        JScrollPane jp = new JScrollPane(mtextarea);
        Container con = mainWindow.getContentPane();

        // Scroll to bottom
/*
        jp.getVerticalScrollBar().addAdjustmentListener(e -> {
            e.getAdjustable().setValue(e.getAdjustable().getMaximum());
        });

        jp.getVerticalScrollBar().remove(0);
*/

        con.add(jp, BorderLayout.CENTER);
        con.add(console, BorderLayout.SOUTH);
        jp.setWheelScrollingEnabled(true);

        pane = new JPanel();
        BorderLayout bord = new BorderLayout();
        pane.setLayout(bord);
        pane.add("North", menu);
        pane.add("West", toolbar);

        // con enthält 2 Elemente
        pane.add("Center", con);

        // Statusbar
        status = new JLabel(my.trans("welcome"));
        status.setBorder(BorderFactory.createLoweredBevelBorder());
        pane.add("South", status);

        // Zum Schluss
        mainWindow.setContentPane(pane);
        mainWindow.pack();
        //System.out.println(mainPosition.x + " " + mainPosition.y);
        mainWindow.setBounds(my.getBankSettings().position.x, my.getBankSettings().position.y, 700, 380);
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setVisible(true);
        mainWindow.toFront();
        mainWindow.setAutoRequestFocus(true);
        mainWindow.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                settingsScreen(false);
                save();
                my.quit();
            }
        });

        // @TODO
        /**
         * Baustelle!!!! Automatischer Login!
         */
//        loginProcess("waldi", "w");
    }


    /**
     * Abspeichern der Settings selected ldbindex:
     *
     * @apiNote 0 = MySQL
     * @apiNote 1 = Oracle
     * @apiNote 2 = MSSQL
     */
    private void save() {
        String[] data = new String[10];
        Integer index = dropDown.getSelectedIndex();
        data[0] = index.toString();
        data[1] = dbdriver.getText();
        data[2] = dbhost.getText();
        data[3] = dbport.getText();
        data[4] = dbuser.getText();
        data[5] = String.valueOf(dbpass.getPassword());
        data[6] = dbdatabase.getText();
        data[7] = my.getLang().getLangStr();
        data[8] = String.valueOf(mainWindow.getX());
        data[9] = String.valueOf(mainWindow.getY());
        my.saveSettings(data);
        mtextarea.append("\n" + my.trans("saveset"));
    }

    private JMenuBar menue() {
        JMenuBar menueLeiste = new JMenuBar();

        // Menuleiste 1. Elemente
        JMenu datei = new JMenu(my.trans("file"));
        datei.setMnemonic(KeyEvent.VK_D);

        JMenu extras = new JMenu(my.trans("extras"));
        extras.setMnemonic(KeyEvent.VK_X);

        JMenu hilfe = new JMenu(my.trans("help"));
        hilfe.setMnemonic(KeyEvent.VK_H);

        // Untermenüelemente erzeugen
        JMenuItem anmelden = new JMenuItem(my.trans("login"),
                new ImageIcon(getClass().getResource(MyConstants.getImagePath() + "logon.png")));
        anmelden.setMnemonic(KeyEvent.VK_A);
        anmelden.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, Event.ALT_MASK));
        anmelden.setActionCommand("anmelden");
        anmelden.addActionListener(this);

        JMenuItem abmelden = new JMenuItem(my.trans("logoff"),
                new ImageIcon(getClass().getResource(MyConstants.getImagePath() + "logoff.png")));
        abmelden.setMnemonic(KeyEvent.VK_M);
        abmelden.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, Event.ALT_MASK));
        abmelden.setActionCommand("abmelden");
        abmelden.addActionListener(this);

        JMenuItem beenden = new JMenuItem(my.trans("quit"),
                new ImageIcon(getClass().getResource(MyConstants.getImagePath() + "quit.png")));
        beenden.setMnemonic(KeyEvent.VK_B);
        beenden.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, Event.ALT_MASK));
        beenden.setActionCommand("beenden");
        beenden.addActionListener(this);

        JMenuItem settings = new JMenuItem(my.trans("settings"),
                new ImageIcon(getClass().getResource(MyConstants.getImagePath() + "settings.png")));
        settings.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK));
        settings.setActionCommand("settings");
        settings.addActionListener(this);

        JMenuItem conntest = new JMenuItem(my.trans("conntest"),
                new ImageIcon(getClass().getResource(MyConstants.getImagePath() + "conntest.png")));
        conntest.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, Event.ALT_MASK));
        conntest.setActionCommand("conntest");
        conntest.addActionListener(this);

        JMenu langChange = new JMenu("Language (needs restart)");
        langChange.setIcon(new ImageIcon(getClass().getResource(MyConstants.getImagePath() + "world.png")));

        ButtonGroup glanguage = new ButtonGroup();

        MyLocales locales = new MyLocales();

        JRadioButtonMenuItem[] item = new JRadioButtonMenuItem[locales.getSize()];

        int i = 0;
        for(Map.Entry<String, String> entry: locales.getLocales().entrySet()) {
            item[i] = new JRadioButtonMenuItem(entry.getValue());
            item[i].setActionCommand("lang");
            if (my.getLang().getLangStr().equals(entry.getKey())) {
                item[i].setSelected(true);
            }
            item[i].addActionListener(this);
            glanguage.add(item[i]);
            langChange.add(item[i]);
            i++;
        }

        JMenuItem faq = new JMenuItem("F.A.Q.",
                new ImageIcon(getClass().getResource(MyConstants.getImagePath() + "faq.png")));
        faq.addActionListener(this);
        JMenuItem about = new JMenuItem("About",
                new ImageIcon(getClass().getResource(MyConstants.getImagePath() + "info2.png")));
        about.setActionCommand("about");
        about.addActionListener(this);

        // Untermenüelemente hinzufügen
        datei.add(anmelden);
        datei.add(abmelden);
        datei.add(beenden);
        extras.add(settings);
        extras.add(conntest);
        extras.addSeparator();
        extras.add(langChange);
        hilfe.add(faq);
        hilfe.add(about);

        // Hauptmenüelemente hinzufügen
        menueLeiste.add(datei);
        menueLeiste.add(extras);
        menueLeiste.add(hilfe);
        return menueLeiste;
    }

    public void scrollToBottom(boolean scroll) {
        JScrollPane jp = (JScrollPane) mtextarea.getParent().getParent();

        if (scroll) {
            jp.getVerticalScrollBar().addAdjustmentListener(e -> e.getAdjustable().setValue(e.getAdjustable().getMaximum()));
        } else {
            jp.getVerticalScrollBar().removeAdjustmentListener(adj -> {
            });
        }
    }

    private void settingsScreen(boolean real) {
        /*
        SettingsDialog settingsDialog = new SettingsDialog(my.getBankSettings(), my.getLang());
        settingsDialog.setMethodName(getCalledMethod());
        settingsDialog.addConfirmListener(al -> {
            settingsDialog.dispose();

            al.getActionCommand();
            //loginProcess(loginDialog.getUsername(), loginDialog.getUserPassword());
        });
        */


        if (real) {
            temp = new JDialog(mainWindow, true);
            temp.setTitle(my.trans("settings"));
            Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource(MyConstants.getImagePath() + "settings.png"));
            temp.setIconImage(img);
        }

        dropDown = new JComboBox<>();
        Vector<String> dbtyp = new Vector<>();
        dbtyp.add("MySQL");
        dbtyp.add("Oracle");
        dbtyp.add("MSSQL");
        DefaultComboBoxModel<String> comboBoxModel1 = new DefaultComboBoxModel<>(dbtyp);
        dropDown.setModel(comboBoxModel1);
        dropDown.setActionCommand("sett_seldb");
        dropDown.addActionListener(this);

        dbdriver = new JTextField(80);
        dbhost = new JTextField(20);
        dbport = new JTextField(8);
        dbuser = new JTextField(20);
        dbpass = new JPasswordField(20);
        dbdatabase = new JTextField(20);


        dropDown.setSelectedIndex(my.getBankSettings().dbindex);

        // Achtung! Hier wird der "händisch eingetragene" Treiber
        // reingeschrieben!
        dbdriver.setText(my.getBankSettings().dbdriver);
        dbhost.setText(my.getBankSettings().dbhost);
        dbport.setText(my.getBankSettings().dbport);
        dbuser.setText(my.getBankSettings().dbuser);
        dbpass.setText(my.getBankSettings().dbpass);
        dbdatabase.setText(my.getBankSettings().dbdatabase);

        if (real) {
            // Create the panels
            temp.setLayout(new GridLayout(8, 2));
            temp.add(new JLabel("  " + my.trans("dbtype") + ":", SwingConstants.LEFT));
            temp.add(dropDown);
            temp.add(new JLabel("  " + my.trans("dbdriver") + ":", SwingConstants.LEFT));
            temp.add(dbdriver);
            JLabel a = new JLabel("  " + my.trans("host") + ":", SwingConstants.LEFT);
            temp.add(a);
            a.setToolTipText("  " + my.trans("standard"));
            temp.add(dbhost);
            a = new JLabel("  Port: ", SwingConstants.LEFT);
            temp.add(a);
            a.setToolTipText("  " + my.trans("standard"));
            temp.add(dbport);
            temp.add(new JLabel("  " + my.trans("user") + ":", SwingConstants.LEFT));
            temp.add(dbuser);
            temp.add(new JLabel("  " + my.trans("passwort") + ":", SwingConstants.LEFT));
            temp.add(dbpass);
            temp.add(new JLabel("  " + my.trans("db") + ":", SwingConstants.LEFT));
            temp.add(dbdatabase);

            JButton sett_abbrechen, sett_speichern;
            sett_speichern = new JButton(my.trans("save"));
            sett_speichern.setActionCommand("sett_speichern");
            temp.add(sett_speichern);
            sett_speichern.addActionListener(this);

            sett_abbrechen = new JButton(my.trans("cancel"));
            temp.add(sett_abbrechen);
            sett_abbrechen.setActionCommand("sett_abbrechen");
            sett_abbrechen.addActionListener(this);

            temp.setResizable(false);
            temp.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            temp.setBounds(mainWindow.getX(), mainWindow.getY(), 300, 250);
            dbdriver.requestFocus();
            temp.setVisible(true);
        }
    }

    private String getCalledMethod() {
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        StackTraceElement e = stacktrace[2]; //coz 0th will be getStackTrace so 1st
        return e.getMethodName();
    }

    private void callLoginDialog() {
        LoginDialog loginDialog = new LoginDialog(mainWindow, my.getLang());
        loginDialog.setMethodName(getCalledMethod());
        loginDialog.addConfirmListener(al -> {
            loginDialog.dispose();
            loginProcess(loginDialog.getUsername(), loginDialog.getUserPassword());
        });
    }

    private void bankToolbar() {
        toolbar.removeAll();
        JButton kauswahl = new JButton(my.trans("tb_choose"));
        kauswahl.setFont(new Font("", Font.BOLD, 12));
        kauswahl.setToolTipText(my.trans("tb_first"));
        kauswahl.setActionCommand("auswahlKonto");
        kauswahl.addActionListener(this);
        kauswahl.setMnemonic(KeyEvent.VK_1);

        JButton ktnew = new JButton(my.trans("tb_create"));
        ktnew.setActionCommand("creScreen");
        ktnew.addActionListener(this);
        ktnew.setMnemonic(KeyEvent.VK_2);

        JButton ktdel = new JButton(my.trans("tb_close"));
        ktdel.setMnemonic(KeyEvent.VK_3);

        JButton einzahlen = new JButton(my.trans("tb_in"));
        einzahlen.setActionCommand("einzahlen");
        einzahlen.addActionListener(this);
        einzahlen.setMnemonic(KeyEvent.VK_4);

        JButton abheben = new JButton(my.trans("tb_out"));
        abheben.setActionCommand("auszahlen");
        abheben.addActionListener(this);
        abheben.setMnemonic(KeyEvent.VK_5);

        JButton uberweisung = new JButton(my.trans("tb_transfer"));
        uberweisung.addActionListener(this);
        uberweisung.setMnemonic(KeyEvent.VK_6);

        JButton umsatz = new JButton(my.trans("tb_sales"));
        umsatz.setActionCommand("umsatzZeigen");
        umsatz.addActionListener(this);
        umsatz.setMnemonic(KeyEvent.VK_7);

        JButton kinfo = new JButton(my.trans("tb_info"));
        kinfo.setActionCommand("konto_info");
        kinfo.addActionListener(this);
        kinfo.setMnemonic(KeyEvent.VK_8);

        JButton festgeld = new JButton(my.trans("tb_fix"));
        festgeld.setMnemonic(KeyEvent.VK_F);

        JButton sparbuch = new JButton(my.trans("tb_save"));
        sparbuch.setMnemonic(KeyEvent.VK_S);

        JButton arbeit = new JButton(my.trans("tb_workday"));
        arbeit.setActionCommand("arbScreen");
        arbeit.setMnemonic(KeyEvent.VK_D);
        arbeit.addActionListener(this);

        JCheckBox conon = new JCheckBox(" " + my.trans("tb_console"), false);
        conon.setMnemonic(KeyEvent.VK_K);
        // Console ausblenden
        conon.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JCheckBox cb = (JCheckBox) e.getItemSelectable();
                // Fehler in der GUI, die einzige Methode es hinzukriegen
                if (cb.isSelected()) {
                    console.setVisible(true);
                    console.requestFocus();
                } else {
                    console.setVisible(false);
                }
                // Nonsens ich weiß, aber bisher die einzige Möglichkeit
                mainWindow.setSize(mainWindow.getWidth(), mainWindow.getHeight() + 1);
                mainWindow.setSize(mainWindow.getWidth(), mainWindow.getHeight() - 1);
            }
        });

        toolbar.add(kauswahl);
        toolbar.add(ktnew);
        toolbar.add(ktdel);
        toolbar.addSeparator();
        toolbar.add(einzahlen);
        toolbar.add(abheben);
        toolbar.add(uberweisung);
        toolbar.add(umsatz);
        toolbar.add(kinfo);
        toolbar.addSeparator();
        toolbar.add(festgeld);
        toolbar.addSeparator();
        toolbar.add(sparbuch);
        toolbar.addSeparator();
        toolbar.add(arbeit);
        toolbar.addSeparator();
        toolbar.add(conon);
        toolbar.setRollover(true);
        toolbar.setToolTipText(my.trans("toolbartip"));
        toolbar.setVisible(true);
    }

    private void zahlenScreen(final int richtung) {
        // Ein Konto wurde bereits gewählt
        if (kontoChosen) {
            help = richtung;
            String titel;
            temp = new JDialog(mainWindow, true);
            if (richtung == 1)
                titel = my.trans("tb_in");
            else
                titel = my.trans("tb_out");
            temp.setTitle(titel);
            temp.getContentPane();
            temp.setLayout(new GridLayout(2, 2));
            temp.add(new JLabel("  " + my.trans("amount") + ":"));

            final JButton act = new JButton(my.trans("apply"));
            act.setActionCommand("zahlen");
            act.addActionListener(this);

            // Was für ein Umstand!!!
            // Eine kleine Klasse NUR um die Eingabe zu prüfen
            a = new JTextField(new EingabeCheck(), null, 3);
            a.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                }

                public void keyReleased(KeyEvent e) {
                }

                public void keyPressed(KeyEvent e) {
                    int key = e.getKeyCode();
                    if (key == KeyEvent.VK_ENTER && !a.getText().isEmpty()) {
                        act.doClick(); // Klick emulieren
                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        temp.dispose();
                    }
                }
            });

            if (richtung == 1)
                titel = my.trans("to");
            else
                titel = my.trans("from");
            temp.add(a);
            temp.add(new JLabel("  " + titel + " " + my.trans("knummer") + ": " + my.ko_getNr()));
            temp.add(act);
            temp.setResizable(false);
            temp.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            temp.setBounds(mainWindow.getX(), mainWindow.getY(), 360, 80);
            temp.setVisible(true);
        }
    }

    private void umsatzScreen() {
        if (kontoChosen) {
            // my.ko_choose(my.ko_getNr());
            mtextarea.append(my.ko_umsatz());
        }
    }

    private void createKontoScreen() {
        temp = new JDialog(mainWindow, true);
        temp.setTitle("Kontoart wählen");
        Image img = Toolkit.getDefaultToolkit()
                .getImage(getClass().getResource(MyConstants.getImagePath() + "euro.png"));
        temp.setIconImage(img);

        // Mit dem Listener zwinge ich es auf die gewünschte Größe
        /*
         * temp.addComponentListener(new ComponentListener() {
		 * 
		 * @Override public void componentShown(ComponentEvent arg0) { }
		 * 
		 * @Override public void componentResized(ComponentEvent arg0) {
		 * temp.setBounds(mainWindow.getX(), mainWindow.getY(), 330, 200); }
		 * 
		 * @Override public void componentMoved(ComponentEvent arg0) { }
		 * 
		 * @Override public void componentHidden(ComponentEvent arg0) { } });
		 */

        ImageIcon icon1 = new ImageIcon(
                Toolkit.getDefaultToolkit().getImage(getClass().getResource(MyConstants.getImagePath() + "giro.png")));
        ImageIcon icon2 = new ImageIcon(
                Toolkit.getDefaultToolkit().getImage(getClass().getResource(MyConstants.getImagePath() + "spar.png")));
        ImageIcon icon3 = new ImageIcon(
                Toolkit.getDefaultToolkit().getImage(getClass().getResource(MyConstants.getImagePath() + "fest.png")));

        JPanel comboBoxPane = new JPanel();
        comboBoxPane.setLayout(new BorderLayout());
        final JTextField a1, a2, b1, b2, c1, c2;

        /**
         * GIRO
         */
        JPanel card1 = new JPanel();
        card1.add(new JLabel("Überzug in Prozent / Jahr"));
        card1.add(a1 = new JTextField("15.0", 4));
        card1.add(new JLabel("     Gebühren je Quartal in Euro:"));
        card1.add(a2 = new JTextField("3.00", 4));
        JLabel bla = new JLabel("Bitte beachten Sie: Eingaben mit . statt , zu versehen");
        bla.setFont(new Font("", Font.BOLD, 12));
        card1.add(bla);

        /**
         * SPAR
         */
        JPanel card2 = new JPanel();
        card2.add(new JLabel("Bitte den Zinssatz für"));
        card2.add(new JLabel("die Zinsen / Jahr eingeben"));
        card2.add(b1 = new JTextField("1.5", 4));
        // reserviert, OHNE funk!!!
        b2 = new JTextField("1.5", 3);
        b2.getColumns();

        /**
         * FEST
         */
        JPanel card3 = new JPanel();
        card3.add(new JLabel("Bitte den Zinssatz für"));
        card3.add(new JLabel("die Zinsen / Jahr eingeben"));
        card3.add(c1 = new JTextField("1.5", 4));
        card3.add(new JLabel("Und die Laufzeit in Jahren"));
        card3.add(c2 = new JTextField("4", 4));

        final JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Girokonto", icon1, card1, "Girokonto");
        tabbedPane.addTab("Sparbuch", icon2, card2, "Sparbuch");
        tabbedPane.addTab("Festgeldkonto", icon3, card3, "Festgeld");

        tabbedPane.setSelectedIndex(0);

		/*
         * Init wert, wenn das Fenster geöffnet wird. Wird später den Index des
		 * Tabs zurückgeben
		 */
        help = 0;
        tabbedPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                help = tabbedPane.getSelectedIndex();
            }
        });

        // Konto erzeugen
        JButton ok = new JButton("anlegen");
        ok.addActionListener(al -> {

            switch (help) {
                case 0:
                    a = a1;
                    b = a2;
                    break;
                case 1:
                    a = b1;
                    b = b2;
                    break;
                case 2:
                    a = c1;
                    b = c2;
                    break;
            }

            // Parameter: help+1, da ich keine "0" Werte haben möchte
            help++;
            if (a.getText().contains(","))
                a.setText(a.getText().replace(',', '.'));
            if (b.getText().contains(","))
                b.setText(b.getText().replace(',', '.'));
            System.out.println(a.getText() + " " + b.getText());

            my.ko_createKonto(help, a.getText(), b.getText());
                /* @TODO: */
        });

        comboBoxPane.add("Center", tabbedPane);
        comboBoxPane.add("South", ok);
        temp.add(comboBoxPane);
        temp.setResizable(false);
        temp.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        temp.setBounds(mainWindow.getX(), mainWindow.getY(), 330, 200);
        temp.setVisible(true);

    }

    private void arbeitScreen() {
        temp = new JDialog(mainWindow, true);
        temp.setTitle(my.trans("set_work"));
        Image img = Toolkit.getDefaultToolkit()
                .getImage(getClass().getResource(MyConstants.getImagePath() + "euro.png"));
        temp.setIconImage(img);
        temp.setLayout(new GridLayout(2, 2));
        temp.add(new JLabel("  " + my.trans("tb_workday") + ": ", SwingConstants.LEFT));

        a = new JTextField(my.k_getArbeit(), 10);
        a.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ESCAPE)
                    temp.dispose();
                if (key == KeyEvent.VK_ENTER) {
                    actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "setDatum"));
                }
            }
        });
        temp.add(a);
        temp.add(new JLabel("  Format: " + my.trans("datestr"), SwingConstants.LEFT));

        JButton ok = new JButton("OK");
        ok.setActionCommand("setDatum");
        ok.addActionListener(this);

        temp.add(ok);
        temp.setResizable(false);
        temp.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        temp.setBounds(mainWindow.getX(), mainWindow.getY(), 260, 80);
        temp.setVisible(true);
    }

    private void auswahlScreen() {
        if (my.k_anzahlKonten() == 0) {
            mtextarea.append("\n" + my.trans("nokonto"));
            return;
        }
        temp = new JDialog(mainWindow, true);
        temp.setTitle(my.trans("tb_choose"));
        temp.setBackground(Color.WHITE);
        temp.setLayout(new BorderLayout());
        JLabel dopp = new JLabel(my.trans("select_action"), SwingConstants.CENTER);
        dopp.setFont(new Font("", Font.BOLD, 14));
        dopp.setBackground(Color.WHITE);
        temp.add(BorderLayout.PAGE_START, dopp);

        // Doppelschritt: 1.Nur die Kontonummern im Array
        final long[] nums = my.k_getNummern();

        // 2. Schritt der komplette Datensatz
        final String[] kontos = my.k_listeKontos();

        DefaultTableModel dtm = new DefaultTableModel() {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(int row, int column) {
                // all cells false
                return false;
            }
        };

        dtm.addColumn("Konto");
        dtm.addColumn("Kontentyp");
        dtm.addColumn("Erstellt");
        dtm.addColumn("Saldo");

        for (String str : kontos) {
            String[] values = str.split("°°");
            // Alternative
            // dtm.addRow(new String[] {
            // values[0],values[1],values[2],values[3]});
            dtm.addRow(values);
        }

        final JTable table = new JTable(dtm);
        // row, col
        table.changeSelection(0, 0, false, false);

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    my.ko_choose(nums[table.getSelectedRow()]);
                    kontoChosen = true;
                    mtextarea.append("\n" + my.trans("chokonto") + ": " + my.ko_getNr());
                    updateStatusText();
                    temp.dispose();
                }
            }
        });

        table.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        my.ko_choose(nums[table.getSelectedRow()]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    kontoChosen = true;
                    mtextarea.append("\n" + my.trans("chokonto") + ": " + my.ko_getNr());
                    updateStatusText();
                    temp.dispose();
                }
                if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    temp.dispose();
                }
            }
        });

        table.setShowVerticalLines(false);
        table.setCellSelectionEnabled(false);
        table.setRowSelectionAllowed(true);

        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.RIGHT);
        table.getColumn("Saldo").setCellRenderer(dtcr);

        JScrollPane scr = new JScrollPane(table);

        temp.add("Center", scr);

        DecimalFormat df = new DecimalFormat("##,##0.00 \u00A4\u00A4");
        String saldo = String.format("%16s\n", df.format(my.k_gesamtSaldo()));

        JLabel ssaldo = new JLabel("Liquide Mittel: " + saldo, SwingConstants.RIGHT);
        ssaldo.setFont(new Font("Lucida Console", Font.PLAIN, 16));
        ssaldo.setBackground(Color.WHITE);

        temp.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(MyConstants.getImagePath() + "euro.png")));
        temp.add("South", ssaldo);
        temp.setResizable(false);
        temp.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        temp.setBounds(mainWindow.getX(), mainWindow.getY(), 600, 80 + kontos.length * 20);
        temp.setVisible(true);
    }

    private void kontostandScreen() {
        if (kontoChosen) {
            mtextarea.append(my.ko_kontoinfo());
        }
    }

    private void updateStatusText() {
        String formArbeit = my.k_getArbeit();
        if (kontoChosen) {
            formArbeit += " | " + my.trans("knummer") + " " + my.ko_getNr();
        }
        status.setText(" " + my.trans("connuser") + " " + my.k_getFullname() + " | " + my.trans("tb_workday") + ": "
                + formArbeit);
    }

    private void changeLanguage(String language) {
        // Ändere NUR wenn NICHT gleiche Sprache wie bisher angeklickt wurde
        if (!my.getLang().getLangStr().equals(language)) {
            my.setLang(language);
            settingsScreen(false);
            save();
            mainWindow.dispose();
            mainWindow.removeAll();
            createAndShowGUI();
        }
    }

    private void loginProcess(String username, String password) {
        // Erst abmelden, dann kommt die Anmeldung
        logoff();

        int lev = my.dbLogin(username, password);

        // Passwort ok, User ist drin
        if (lev == 0) {
            mtextarea.setText(my.trans("user") + " " + my.k_getFullname() + " " + my.trans("userconn"));
            if (temp != null)
                temp.dispose();
            updateStatusText();
            bankToolbar();
        }

        // Verbindungsprobleme
        if (lev == 1) {
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            JOptionPane.showMessageDialog(null, my.trans("noConn"), "Connection Error", JOptionPane.WARNING_MESSAGE);
        }

        // Login falsch
        if (lev == 2) {
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            } catch (Exception ef) {
                ef.printStackTrace();
            }
            JOptionPane.showMessageDialog(null, my.trans("noaccess"), "Error 404", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void logoff() {
        if (my.isLoggedIn()) {
            toolbar.setVisible(false);
            mtextarea.setText(my.trans("user") + " " + my.k_getFullname() + " " + my.trans("logofft") + "\n"
                    + my.trans("welcomever"));
            status.setText(my.trans("logoffs"));
            console.setText("");
            my.dbLogoff();
            kontoChosen = false;
        }
    }

    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        System.out.println(cmd);

        // Achtung: cmd ist case-sensitive!
        if (cmd.equals("anmelden")) {
            callLoginDialog();
        }
        if (cmd.equals("abmelden")) {
            logoff();
        }
        if (cmd.equals("beenden")) {
            settingsScreen(false);
            save();
            my.quit();
        }
        if (cmd.equals("konto_info")) {
            kontostandScreen();
        }
        if (cmd.equals("lang")) {
            JRadioButtonMenuItem item = (JRadioButtonMenuItem) e.getSource();
            String iso2 = item.getText().substring(0,2);
            changeLanguage(iso2);
        }
        if (cmd.equals("sett_seldb")) {
            switch (dropDown.getSelectedIndex()) {
                case 0:
                    dbdriver.setText("org.gjt.mm.mysql.Driver");
                    dbport.setText("3306");
                    break;
                case 1:
                    dbdriver.setText("oracle.jdbc.OracleDriver");
                    dbport.setText("1521");
                    break;
                case 2:
                    dbdriver.setText("mssql.Driver");
                    dbport.setText("1517");
                    break;
                default:
                    dbdriver.setText("PostgreSQL");
                    dbport.setText("5432");
                    break;
            }

        }
        if (cmd.equals("F.A.Q.")) {
            System.out.println("faq wurde angeklickt");
            my.getBankSettings().readSettings();
        }
        if (cmd.equals("settings")) {
            settingsScreen(true);
        }
        if (cmd.equals("about")) {
            new AboutDialog(mainWindow);
        }
        if (cmd.equals("einzahlen")) {
            help = 1;
            zahlenScreen(1);
        }
        if (cmd.equals("umsatzZeigen")) {
            umsatzScreen();
        }
        if (cmd.equals("auszahlen")) {
            help = 0;
            zahlenScreen(0);
        }
        if (cmd.equals("zahlen")) {
            temp.dispose();
            mtextarea.append("\n" + my.barZahlung(help, a.getText()));
        }
        if (cmd.equals("auswahlKonto")) {
            auswahlScreen();
        }
        if (cmd.equals("conntest")) {
            mtextarea.append("\n" + my.trans("connmess") + "\n" + my.trans("plwait") + "\n");
            mtextarea.append(my.connTest());
        }
        if (cmd.equals("creScreen")) {
            createKontoScreen();
        }
        if (cmd.equals("sett_speichern")) {
            save();
        }
        if (cmd.equals("sett_abbrechen") || cmd.equals("sett_speichern")) {
            temp.dispose();
        }
        if (cmd.equals("arbScreen")) {
            arbeitScreen();
        }
        if (cmd.equals("setDatum")) {
            if (!my.k_setArbeit(a.getText())) {
                try {
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                } catch (Exception ef) {
                    ef.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, my.trans("wronginput"), "Error 123",
                        JOptionPane.INFORMATION_MESSAGE);
            } else {
                updateStatusText();
                temp.dispose();
            }
        }
    }

    private void exek(String comm) {
        console.setEnabled(false);
        if (comm.equals("conntest")) {
            mtextarea.append("\n" + my.trans("connmess") + "\n" + my.trans("plwait") + "\n");
            mtextarea.append(my.connTest());
        }
        if (comm.equals("on")) {
            callLoginDialog();
        }
        if (comm.equals("about"))
            new AboutDialog(mainWindow);
        if (comm.equals("set"))
            settingsScreen(true);
        if (comm.equals("size"))
            mainWindow.setBounds(70, 70, 600, 380);
        if (comm.equals("saveconn"))
            save();
        if (comm.equals("getconn"))
            my.getBankSettings().readSettings();
        if (comm.equals("syes"))
            scrollToBottom(true);
        if (comm.equals("sno"))
            scrollToBottom(false);
        if (comm.equals("coords")) {
            Point p = mainWindow.getLocation();
            System.out.println("x: " + p.x + " y: " + p.y);
        }
        if (my.connected()) {
            if (comm.equals("off"))
                logoff();
            if (comm.equals("saldo"))
                mtextarea.append("\n" + my.k_gesamtSaldo());
            if (comm.equals("sel"))
                auswahlScreen();
            if (comm.equals("ein"))
                zahlenScreen(1);
            if (comm.equals("aus"))
                zahlenScreen(0);
            if (comm.equals("kon"))
                kontostandScreen();
            if (comm.equals("ums"))
                umsatzScreen();
            if (comm.equals("con"))
                mtextarea.append("\nKonten: " + my.k_anzahlKonten());
            // Arbeitsdatum einstellen
            if (comm.equals("work"))
                arbeitScreen();

        }

        if (comm.equals("cls"))
            mtextarea.setText(my.trans("welcomever") + "\n");
        console.setEnabled(true);
        console.requestFocus();
        if (comm.equals("exit") || comm.equals("x"))
            if (my.connected()) {
                my.quit();
            }
    }

    @Override
    public void run() {
        javax.swing.SwingUtilities.invokeLater(() -> {
            createAndShowGUI();
        });
    }
}
