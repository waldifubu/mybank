package view;

import model.BankSettings;
import model.Lang;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Waldi on 05.02.2016.
 */
public class SettingsDialog extends ScreenDialog{

    private BankSettings bankSettings;
    private JButton sett_save;

    public SettingsDialog(BankSettings bankSettings, Lang language) {
        sett_save = new JButton(language.getValue("save"));

    }


    public void addConfirmListener(ActionListener listener) {
        sett_save.addActionListener(listener);
    }

}
