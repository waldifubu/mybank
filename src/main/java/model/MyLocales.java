package model;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Waldi on 04.03.2016.
 */
public class MyLocales {

    private HashMap<String, String> locales;

    public MyLocales() {
        locales = new HashMap<>();
        String str = getClass().getResource(MyConstants.getLangPath()).toString() + ".";

        File dir = null;
        try {
            dir = new File(new URI(str));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        if(dir != null) {
            File[] filesList = dir.listFiles();
            String iso2;
            Locale locale;
            for (File file : filesList) {
                if (file.isFile() && file.getName().contains(".properties")) {
                    iso2 = file.getName().replaceFirst("[.][^.]+$", "");
                    locale = new Locale(iso2);
                    locales.put(iso2, iso2 + " " + locale.getDisplayLanguage(locale));
                }
            }
        }
    }

    public HashMap<String, String> getLocales() {
        return locales;
    }

    public int getSize() {
        return locales.size();
    }

    public void output() {
        for(Map.Entry<String, String> entry: locales.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }
    }
}
