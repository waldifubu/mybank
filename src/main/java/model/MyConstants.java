package model;

final public class MyConstants {

    private static final String connectionFile = "settings.cfg";
    private static final String imagePath = "/images/";
    private static final String langPath = "/lang/";
    private static final String rulesFile = "rules.xml";

    public static String getConnectionFile() {
        return connectionFile;
    }

    public static String getImagePath() {
        return imagePath;
    }

    public static String getLangPath() {
        return langPath;
    }

    public static String getRulesFile() {
        return rulesFile;
    }
}
