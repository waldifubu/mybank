package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

public class Lang {

    private final String suffix = ".properties";
    private Properties props = new Properties();
    private String langStr;

    public Lang(String lang) {

//        file_check(lang);
        lang_open(lang);
        langStr = lang;
    }

    /**
     * @deprecated
     * @param lang
     */
    private void file_check(String lang) {
        String path = MyConstants.getLangPath() + lang + suffix;
        URL url = getClass().getResource(path);

        if (url == null) {
            File f = new File("bin/resources/lang/" + lang + suffix);
            try {
                if (!f.exists())
                    f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                try {
                    f.createNewFile();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    /**
     * @deprecated
     * @param lang
     */
    private void lang_open(String lang) {
        try {
            InputStream u = getClass().getResourceAsStream(MyConstants.getLangPath() + lang + suffix);
            // props.loadFromXML(u);
            props.load(u);
        } catch (java.util.InvalidPropertiesFormatException ex) {
            // Ungültige Daten, wird aber autom. behoben
        } catch (IOException e) {
            // e.printStackTrace();
            System.exit(0);
        }
    }

    public boolean redundancy_check(String key) {
        Enumeration<?> emu = props.propertyNames();
        String altkey = null;
        while (emu.hasMoreElements()) {
            altkey = (String) emu.nextElement();
            if (altkey.equals(key)) {
                System.out.println("Already used key - " + key);
                return true;
            }

        }
        return false;
    }

    public String getValue(String key) {
        return props.getProperty(key);
    }

    public int size() {
        return props.size();
    }

    public void add(String a, String b) {
        // if (!redundancy_check(a))
        props.setProperty(a, b);
    }

    public void save(String lang) {
        /*
		 * props.setProperty("welcomever", "Willkommen zu myBank 1.0!");
		 */
        File f = new File(getClass().getResource(MyConstants.getLangPath() + lang + suffix).getFile());
        f.delete();

        try {
            props.store(new FileOutputStream(f), "Language file v1.0.0 for language type " + lang);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getLangStr() {
        return langStr;
    }
}
