package model;

import org.jasypt.util.text.BasicTextEncryptor;

import javax.swing.*;
import java.awt.*;
import java.io.*;

/**
 * Created by Waldi on 05.02.2016.
 */
public class BankSettings {

    public int dbindex;
    public String dbhost;
    public String dbdatabase;
    public String dbdriver;
    public String dbuser;
    public String dbport;
    public String dbpass;
    public String slanguage;

    // Postion of mainWindow
    public Point position;
    private final String SECRETKEY = "Waldi";
    private static BankSettings ourInstance = new BankSettings();

    public static BankSettings getInstance() {
        return ourInstance;
    }

    private BankSettings() {
        readSettings();
    }

    public String[] getDbSettings() {
        String[] data = new String[7];
        Integer index = dbindex;
        data[0] = index.toString();
        data[1] = dbdriver;
        data[2] = dbhost;
        data[3] = dbport;
        data[4] = dbuser;
        data[5] = dbpass;
        data[6] = dbdatabase;
        return data;
    }

    public void setSettings(String[] allsettings) {
        dbindex = Integer.parseInt(allsettings[0]);
        dbdriver = allsettings[1];
        dbhost = allsettings[2];
        dbport = allsettings[3];
        dbuser = allsettings[4];
        dbpass = allsettings[5];
        dbdatabase = allsettings[6];
        slanguage = allsettings[7];
        position.setLocation(Integer.parseInt(allsettings[8]), Integer.parseInt(allsettings[9]));
    }

    public void readSettings() {
        position = new Point(70, 70);
        try {
            DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(MyConstants.getConnectionFile())));
            dbindex = in.readInt();
            dbdriver = in.readUTF();
            dbhost = in.readUTF();
            dbport = in.readUTF();
            dbuser = in.readUTF();
            BasicTextEncryptor bte = new BasicTextEncryptor();
            bte.setPassword(SECRETKEY);
            dbpass = bte.decrypt(in.readUTF());
            dbdatabase = in.readUTF();
            slanguage = (in.readUTF());
            position.setLocation(in.readInt(), in.readInt());
            in.close();
        } catch (Exception ignore) {
            File f = new File(MyConstants.getConnectionFile());
            System.gc();
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            } catch (Exception e) {
                e.printStackTrace();
            }
            JOptionPane.showMessageDialog(null, "Sorry, Connection data are missing!\nCreate new file for settings.",
                    "Ooops: Error 500", JOptionPane.ERROR_MESSAGE);
            f.delete();

            // sonst läuft das prog Amok ;-)
            dbindex = 0;
            slanguage = "en";
        }
    }

    public void saveSettings() {
        BasicTextEncryptor bte = new BasicTextEncryptor();
        bte.setPassword(SECRETKEY);
        String encdbpass = bte.encrypt(dbpass);
        try {
            DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(MyConstants.getConnectionFile())));
            out.writeInt(dbindex);
            out.writeUTF(dbdriver);
            out.writeUTF(dbhost);
            out.writeUTF(dbport);
            out.writeUTF(dbuser);
            out.writeUTF(encdbpass);
            out.writeUTF(dbdatabase);
            out.writeUTF(slanguage);
            out.writeInt(position.x);
            out.writeInt(position.y);
            out.close();
        } catch (IOException fatal) {
            fatal.printStackTrace();
        }
    }

    public String getSlanguage() {
        return slanguage;
    }
}
