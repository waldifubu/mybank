package model;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBase {

    // Connection init
    private Connection conn;

    // DB data
    private String ldbhost;
    private String ldbuser;
    private String ldbpass;
    private String ldbdatabase;
    private String ldbdriver;

    // Type of db
    private int ldbindex;

    private int ldbport;

    // Verbindungstest: true / false
    private boolean online;

    // Connection String
    private String dbUrl;

    // DB Query variables
    private Statement st;

    public DBase(String[] data) {
        try {
            ldbindex = Integer.parseInt(data[0]);
            ldbdriver = data[1];
            ldbhost = data[2];
            ldbport = Integer.parseInt(data[3]);
            ldbuser = data[4];
            ldbpass = data[5];
            ldbdatabase = data[6];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isOnline() {
        return online;
    }


    public String connectTest() {
        //S1: Treiber laden
        StringBuilder response = new StringBuilder("Step 1: Loading driver... ");
        try {
            Class.forName(ldbdriver).newInstance();
            response.append("driver is loaded\n");
        } catch (Exception e) {
            response.append("Unable to load driver\nPlease get the right driver\n");
            return response.toString();
        }

        //S2: Gucken ob Server läuft
        try {
            long tm = System.nanoTime();
            response.append("Step 2: Checking host... ");
            InetAddress host = InetAddress.getByName(ldbhost);
            int port = ldbport;
            Socket so = new Socket(host, port);
            so.close();
            tm = (System.nanoTime() - tm) / 1000000L;
            response.append("isOnline\n    Host Address = ").append(host.getHostAddress()).append("\n").append("    Host Name    = ")
                    .append(host.getHostName()).append("\n").append("Response Time = ").append(tm).append(" ms\n");
        } catch (IOException es) {
            //2b: Wir testen ob Webserver läuft
            response.append("Connection is dead\n");
            try {
                InetAddress host;
                host = InetAddress.getByName(ldbhost);
                Socket so = new Socket(host, 80);
                so.close();
                response.append("Webserver is running, but no DBMS\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        //S3: Verbindung aufbauen, bis hier war alles ok!
        try {
            response.append("Step 3: Trying to connect to DB... ");
            /**
             * selected index:
             * @param 0 = MySQL
             * @param 1 = Oracle
             * @param 2 = MSSQL
             **/
            switch (ldbindex) {
                case 0:
                    dbUrl = "jdbc:mysql://" + ldbhost + ":" + ldbport + "/" + ldbdatabase;
                    break;
            }
            conn = DriverManager.getConnection(dbUrl, ldbuser, ldbpass);
            response.append("Connection established\n");

            // Ohne Anmeldung wird die Verbindung geschlossen, da es nur ein Test ist
            if (!online) conn.close();

            //TRUE Verbindung möglich
            online = true;
            return response.toString();
        } catch (com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException e) {
            if (e.getErrorCode() == 1049) response.append("Database is not accessible!\n");
            return response.toString();
        } catch (SQLException e) {
            return "Fatal error: Error in settings!";
        }
    } // Conn Meth Ende


    public boolean alter(String abfrage) {
        try {
            st = conn.createStatement();
            st.executeUpdate(abfrage);
            st.close();
            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    public ResultSet myQuery(String abfrage) {
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(abfrage);
            if (rs.wasNull()) return null;
            return rs;
        } //try
        catch (Exception ex) {
            ex.getMessage();
        }
        return null;
    }

    public void close() {
        try {
            online = false;
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean userConn(String user, String pass) {
        // Jetzt werden die Logindaten ausgewertet
        try {
            Class.forName(ldbdriver).newInstance();
            conn = DriverManager.getConnection(dbUrl, ldbuser, ldbpass);
        } catch (Exception e) {
            return false;
        }

        try {
            ResultSet rs = myQuery("select u_pass from users where u_nick='" + user + "'");
            String fund;
            while (rs.next()) {
                fund = rs.getString("u_pass");
                if (rs.wasNull()) return false;
                else {
                    if (pass.equals(fund)) {
                        rs.close();
                        return true;
                    }
                }
            } //while
        } //try

        catch (SQLException ex) {
            System.out.println("SQL-Error: Error not found");
        }
        return false;
    } // Meth ende


}
